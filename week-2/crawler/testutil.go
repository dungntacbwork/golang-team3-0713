package crawler

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

func generateRes(filePath, url string) *http.Response {
	w := httptest.NewRecorder()
	data, _ := ioutil.ReadFile(filePath)
	w.Write(data)
	resp := w.Result()
	req := httptest.NewRequest("GET", url, nil)
	resp.Request = req

	return resp
}
