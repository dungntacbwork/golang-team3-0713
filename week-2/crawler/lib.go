package crawler

import (
	"net/http"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Selector struct {
	Title     string
	PublishAt string
	Author    string
	Content   string
}

type Data struct {
	Title     string
	PublishAt time.Time
	Author    string
	Content   string
}

type ICrawler interface {
	Parse(res *http.Response) Data
}

type Crawler struct {
	selector Selector
	parser   Parser
}

func (that *Crawler) Parse(res *http.Response) Data {
	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		panic(err)
	}
	data := Data{}
	data.Title = that.parser.extractTitle(that.selector.Title, doc)
	data.Author = that.parser.extractAuthor(that.selector.Author, doc)
	data.Content = that.parser.extractContent(that.selector.Content, doc)
	data.PublishAt = that.parser.extractPublishAt(that.selector.PublishAt, doc)
	return data
}
