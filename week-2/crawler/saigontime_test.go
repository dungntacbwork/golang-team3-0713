package crawler

import "testing"

func Test_Crawl(t *testing.T) {
	fileDataPath := "./data/thesaigontimes.html"
	url := "https://www.thesaigontimes.vn/291878/vi-sao-thi-truong-mua-ban-dien-mat-troi-roi-loan.html"
	resp := generateRes(fileDataPath, url)

	var SaiGonTime ICrawler = CreateSGTimeCrawler()
	data := SaiGonTime.Parse(resp)

	if data.Title != "Vì sao thị trường mua bán điện mặt trời rối loạn" {
		t.Errorf("Title should be expected")
	}
}
