package crawler

import (
	"time"

	"github.com/PuerkitoBio/goquery"
)

type ExtracyString = func(selector string, doc *goquery.Document) string
type ExtracyTime = func(selector string, doc *goquery.Document) time.Time
type Parser struct {
	extractTitle     ExtracyString
	extractContent   ExtracyString
	extractPublishAt ExtracyTime
	extractAuthor    ExtracyString
}

func CreateParser() Parser {
	parser := Parser{
		extractPublishAt: extractNotYetImp,
		extractContent:   extractSimpleString,
		extractAuthor:    extractSimpleString,
		extractTitle:     extractSimpleString,
	}
	return parser
}

func extractSimpleString(selector string, doc *goquery.Document) string {
	return doc.Find(selector).Text()
}

func extractNotYetImp(selector string, doc *goquery.Document) time.Time {
	panic("Should implement at yours")
}

func extractPublishDateSGTime(selector string, doc *goquery.Document) time.Time {
	return time.Now()
}
