package crawler

type impSaiGonTime struct {
	Crawler
}

func CreateSGTimeCrawler() ICrawler {
	selector := Selector{
		Title:     ".Title",
		PublishAt: "ctl00_cphContent_lblCreateDate",
		Author:    "ctl00_cphContent_Lbl_Author",
		Content:   ".Content",
	}
	crawler := &impSaiGonTime{}
	crawler.selector = selector
	crawler.parser = CreateParser()
	crawler.parser.extractPublishAt = extractPublishDateSGTime
	return crawler

}
