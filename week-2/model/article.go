package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Article struct {
	gorm.Model
	UrlId     uint
	Title     string
	PublishAt time.Time
	Content   string
	Author    string
	Status    int
}
