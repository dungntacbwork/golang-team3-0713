package main

import (
	"./model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1. Define DB

// 2. Func read db

// 3. Craw content

// 4. Parse Article

// 5. Insert data to DB
func main() {
	db, err := gorm.Open("mysql", "default:secret@/crawler-abc?charset=utf8")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.AutoMigrate(&Product{})
	articleChan := crawl(urlChan)
}

func load(db *gorm.DB) <-chan model.Url {
	urlChan := make((chan model.Url), 10)
	
	go func() {
		for {
			urls := []model.Url{}
			err:= db.Where("state = ?", model.UrlState)
			.Limit(10)
			.Find(&urls)
			if err != nil{
				time.Sleep(5* time.Second)
				continue
			}
			time.Sleep(1 * time.Second)
			for index := 0; index < len(urls); index++ {
				urlChan <- urls[i]
			}
		}
		
	}()
	return urlChan
}

func crawl(urlChan <- chan model.Url)<- model.Article  {
	articleChan := make(chan model.Article, 10)
	return articleChan
	
}
