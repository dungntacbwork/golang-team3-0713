package services

import (
	m "../models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// VoucherRoutes List Route API
func VoucherRoutes(engine *gin.Engine, db *gorm.DB) {
	groupRouter := engine.Group("/voucher")
	{
		groupRouter.POST("", func(c *gin.Context) {
			repo := &m.VoucherBo{
				DB: db,
			}
			result, err := m.CreateVoucher(c, repo)
			ConvertDataResult(c, err, result)
		})
		groupRouter.GET("/verify/:code", func(c *gin.Context) {
			repo := &m.VoucherBo{
				DB: db,
			}
			result, err := m.VerifyVoucher(c, repo)
			ConvertDataResult(c, err, result)
		})
		groupRouter.PATCH("/:code/claim", func(c *gin.Context) {
			repo := &m.VoucherBo{
				DB: db,
			}
			result, err := m.ClaimVoucher(c, repo)
			ConvertDataResult(c, err, result)
		})
	}
}

// ConvertDataResult convert data trả về
func ConvertDataResult(c *gin.Context, err error, result interface{}) {
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(200, result)
}
