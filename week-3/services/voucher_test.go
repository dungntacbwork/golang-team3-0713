package services

import (
	"net/http/httptest"
	"strings"

	"github.com/gin-gonic/gin"
)

func buildMockContext(method string, path string, data string) *gin.Context {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest(method, path, strings.NewReader(data))
	ctx.Request.Header.Set("Content-Type", "application/json")
	return ctx
}

// func Test_CreateVoucher(t *testing.T) {
// 	gin.SetMode(gin.ReleaseMode)
// 	// 1. Chuan bi input dau vao cho ham CreateNote
// 	data := `{"code": "AAAAAAA","value": 1, "startDate":"2019-01-01T00:00:00.000Z", "endDate":"2020-01-01T00:00:00.000Z", "max":10000}`
// 	ctx := buildMockContext("POST", "/voucher", data)
// 	repo := new(mock.VoucherBo)
// 	_, err := models.CreateVoucher(ctx, repo)
// 	if err != nil {
// 		t.Fail()
// 	}
// }
