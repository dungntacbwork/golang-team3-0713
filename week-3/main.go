package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"./models"
	"./services"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	db, err := gorm.Open("mysql", "root:password@/voucherDb?charset=utf8&parseTime=True&loc=Local")
	if err == nil {
		panic(err)
	}
	fmt.Println("vvvvvv")
	defer db.Close()
	fmt.Println("aaaaaa")
	db.LogMode(true)
	db.AutoMigrate(&models.Voucher{})

	fileWriter, err := os.Create("access.log")
	if err != nil {
		panic(err)
	}
	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = fileWriter

	r := gin.Default()
	services.VoucherRoutes(r, db)
	port := os.Getenv("HTTP_PORT")
	if port == "" {
		port = "8085"
	}

	srv := &http.Server{
		Addr:    ":" + port,
		Handler: r,
	}

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			panic(err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	srv.Shutdown(ctx)
}
