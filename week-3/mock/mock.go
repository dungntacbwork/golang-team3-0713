package mock

import (
	"errors"

	"../models"
	"github.com/stretchr/testify/mock"
)

// VoucherBo Mock object
type VoucherBo struct {
	mock.Mock
}

// Create mock create voucher
func (that *VoucherBo) Create(vc models.Voucher) (*models.Voucher, error) {
	if len(vc.Code) == 0 {
		return &vc, errors.New("Code is empty")
	}
	if len(vc.Code) < 6 || len(vc.Code) > 10 {
		return &vc, errors.New("Code Out Of Len. (6-10)")
	}
	return &vc, nil
}

func (that *VoucherBo) Validate(vc models.Voucher) (*models.Voucher, error) {
	if len(vc.Code) == 0 {
		return &vc, errors.New("Code is empty")
	}
	if len(vc.Code) < 6 || len(vc.Code) > 10 {
		return &vc, errors.New("Code Out Of Len. (6-10)")
	}
	return &vc, nil
}
