package models

import (
	"errors"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// Voucher object Voucher
type Voucher struct {
	gorm.Model
	Code      string    `binding:"required,min=6,max=10"`
	Value     float32   `binding:"required"`
	StartDate time.Time `binding:"required"`
	EndDate   time.Time `binding:"required"`
	Max       int       `binding:"required"`
	Claimed   int
	Spending  bool
}

// VoucherRes object Response
type VoucherRes struct {
	ID        uint
	Code      string
	Value     float32
	StartDate time.Time
	EndDate   time.Time
	Max       int
}

// NewVoucher khởi tạo object Voucher
func NewVoucher() Voucher {
	voucher := Voucher{}
	voucher.Claimed = 0
	voucher.Spending = false
	return voucher
}

// CreateVoucher Tạo Voucher
func CreateVoucher(c *gin.Context, repo IVoucher) (*VoucherRes, error) {
	voucher := Voucher{}
	if err := c.ShouldBind(&voucher); err != nil {
		return nil, err
	}
	voucher.Spending = false
	voucher.Claimed = 0
	valid, err := repo.Validate(voucher)
	if valid && err == nil {
		return nil, errors.New("Mã code đã tồn tại trong khoảng thời gian đã chọn")
	}
	createdVoucher, err := repo.Create(voucher)
	if err != nil {
		return nil, err
	}
	result := &VoucherRes{
		ID:        createdVoucher.ID,
		Code:      createdVoucher.Code,
		Value:     createdVoucher.Value,
		EndDate:   createdVoucher.EndDate,
		StartDate: createdVoucher.StartDate,
		Max:       createdVoucher.Max,
	}
	return result, nil
}

// VerifyVoucher Kiem tra voucher
func VerifyVoucher(c *gin.Context, repo IVoucher) (bool, error) {
	vc := Voucher{}
	vc.Code = c.Param("code")
	vc.StartDate = time.Now()
	vc.EndDate = time.Now().Add(8600400)
	vaid, err := repo.Validate(vc)
	if vaid && err == nil {
		return true, nil
	}
	return false, errors.New("Voucher hết hiệu lực")
}

//ClaimVoucher Claim Voucher
func ClaimVoucher(c *gin.Context, repo IVoucher) (bool, error) {
	vc := Voucher{}
	vc.Code = c.Param("code")
	vc.StartDate = time.Now()
	vc.EndDate = time.Now().Add(8600400)
	vaid, err := repo.Claim(vc)
	if vaid && err == nil {
		return true, nil
	}
	return false, err
}

// IVoucher Tao Interface
type IVoucher interface {
	Create(Voucher) (*Voucher, error)
	Validate(Voucher) (bool, error)
	Claim(Voucher) (bool, error)
}

// VoucherBo Tạo BusinessObject
type VoucherBo struct {
	DB *gorm.DB
}

// Create Insert Voucher to Db
func (that *VoucherBo) Create(vc Voucher) (*Voucher, error) {
	err := that.DB.Create(&vc).Error
	return &vc, err
}

// Validate data
func (that *VoucherBo) Validate(vc Voucher) (bool, error) {
	voucher := Voucher{}
	err := that.DB.Where("StartDate >= ? AND Endate <= ? And Code = ? && Max >= Claimed", vc.StartDate, vc.EndDate, vc.Code).First(&voucher).Error
	if err == nil {
		return false, err
	}
	return true, err
}

// Claim Nhận quà
func (that *VoucherBo) Claim(vc Voucher) (bool, error) {
	voucher := Voucher{}
	err := that.DB.Where("StartDate >= ? AND Endate <= ? And Code = ? && Max >= Claimed", vc.StartDate, vc.EndDate, vc.Code).First(&voucher).Error
	voucher.Claimed++
	if err != nil || voucher.Claimed > voucher.Max {
		return false, errors.New("Voucher vừa hết")
	}
	that.DB.Save(&voucher)
	return true, nil
}
